/// <reference path='definitions/jquery.d.ts' />
/// <reference path='definitions/greensock.d.ts' />
/// <reference path='definitions/fastclick.d.ts' />
/// <reference path='definitions/hammerjs.d.ts' />
/// <reference path='definitions/owlcarousel.d.ts' />

$(document).ready(function(){
	var menuIsOpen: Boolean = false;

	$(".carousel .owl-carousel").owlCarousel({
 
		// Most important owl features
		singleItem: true,

		responsive: true
	});

	var owl = $(".carousel .owl-carousel").data('owlCarousel');

	$('.controls .left').click(function() {
		owl.prev();
	});

	$('.controls .right').click(function() {
		owl.next();
	});

	$('.hamburger').click(function(){
		console.log('click');

		if(menuIsOpen){
			TweenLite.to('header', 0.4, { height: 60 });
			menuIsOpen = false;
		}else{
			TweenLite.to('header', 0.4, { height: $(window).height() });
			menuIsOpen = true;
		}
	})
})