var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var clean = require('gulp-clean');
var ejs = require("gulp-ejs");
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var ts = require('gulp-typescript');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var merge = require('merge-stream');
var uncss = require('gulp-uncss');
var browserSync = require('browser-sync').create();

var vendorFiles = [
	'src/scripts/vendor/jquery-1.11.3.js', 
	'src/scripts/vendor/greensock/TweenLite.js',
	'src/scripts/vendor/greensock/plugins/CSSPlugin.js',
	'src/scripts/vendor/fastclick.js',
	'src/scripts/vendor/owl.carousel.js'
]

// Clean Tasks
///////////////////////////////////////////////////////////////////////////
gulp.task('clean-all', function () {
	return gulp.src('build', {read: false})
		.pipe(clean());
});

gulp.task('clean-html', function () {
	return gulp.src('build/views', {read: false})
		.pipe(clean());
});

gulp.task('clean-styles', function () {
	return gulp.src('build/styles', {read: false})
		.pipe(clean());
});

gulp.task('clean-scripts', function () {
	return gulp.src('build/scripts', {read: false})
		.pipe(clean());
});

// Dev Tasks
///////////////////////////////////////////////////////////////////////////
gulp.task('html', ['clean-html'], function(){
	var home = gulp.src("*.ejs")
		.pipe(ejs())
		.pipe(gulp.dest(""));

	var src = gulp.src("src/**/*.ejs")
		.pipe(ejs())
		.pipe(gulp.dest("build"));

	return merge(home, src);
});

gulp.task('styles', ['clean-styles'], function(){
	return gulp.src('src/styles/app.scss')
		.pipe(sourcemaps.init())
		.pipe(autoprefixer())
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(browserSync.stream())
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest('build/styles'));
});

gulp.task('scripts', ['clean-scripts'], function(){
	var typescript = gulp.src('src/**/*.ts')
		.pipe(sourcemaps.init())
		.pipe(ts({
			noImplicitAny: true
		}))
		.pipe(uglify())
		.pipe(sourcemaps.write('maps'))
		.pipe(gulp.dest('build'));

	var vendor = gulp.src(vendorFiles)
	    .pipe(concat('vendor.js'))
	    .pipe(gulp.dest('build/scripts'));

	return merge(typescript, vendor);
});


// Distribution Tasks
///////////////////////////////////////////////////////////////////////////
gulp.task('html-dist', ['clean-all'], function(){
	var home = gulp.src("*.ejs")
		.pipe(ejs())
		.pipe(gulp.dest(""));

	var src = gulp.src("src/**/*.ejs")
		.pipe(ejs())
		.pipe(gulp.dest("build"));

	return merge(home, src);
});

gulp.task('styles-dist', ['clean-all', 'html-dist'], function(){
	return gulp.src('src/styles/app.scss')
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(autoprefixer())
		// .pipe(uncss({
  //           html: ['index.html']
  //       }))
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(gulp.dest('build/styles'));
});

gulp.task('scripts-dist', ['clean-all'], function(){
	var typescript = gulp.src('src/**/*.ts')
		.pipe(ts({
			noImplicitAny: true
		}))
		.pipe(uglify())
		.pipe(gulp.dest('build'));

	var vendor = gulp.src(vendorFiles)
	    .pipe(concat('vendor.js'))
	    .pipe(uglify())
	    .pipe(gulp.dest('build/scripts'));

	return merge(typescript, vendor);
});




// Reload and CSS Injection Tasks
///////////////////////////////////////////////////////////////////////////
gulp.task('html-reload', ['html'], function() {
    browserSync.reload();
});

gulp.task('styles-reload', ['styles'], function() {
    //browserSync.reload();
    //browserSync.stream();
});

gulp.task('scripts-reload', ['scripts'], function() {
    browserSync.reload();
});


// Server and LiveReload Tasks
///////////////////////////////////////////////////////////////////////////



// Run Tasks
///////////////////////////////////////////////////////////////////////////
var devTasks = ['html', 'styles', 'scripts'];
var distTasks = ['html-dist', 'styles-dist', 'scripts-dist'];

gulp.task('serve', devTasks, function () {
	browserSync.init({
        server: {
            baseDir: ""
        },
        open: false
    });

	gulp.watch('*.ejs', ['html-reload']);
	gulp.watch('src/views/**/*', ['html-reload']);
	gulp.watch('src/styles/**/*', ['styles-reload']);
	gulp.watch('src/scripts/**/*', ['scripts-reload']);
});

gulp.task('dist', distTasks);
